# GN1 Tagger

A recent development in the field of machine learning are graph neural networks (GNNs). A GNN is a special kind of neural network which directly operates on the graph structure. Particularly the identification of heavy-flavour jets in collision events provides a fruitful playground to apply GNNs due to the well-structured anatomy of collision events.

A comprehensive introduction with a survey of applications to particle physics is provided in [Jonathan Shlomi et al 2021 Mach. Learn.: Sci. Technol. 2 021001](https://dx.doi.org/10.1088/2632-2153/abbf9a).

The GN1 pub note is available on [CDS](https://cds.cern.ch/record/2811135).

## Code

The training code can be found on the official [FTAG algorithm gitlab](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/GNNJetTagger).

This framework has been replaced by the new [Salt](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/salt) framework.

