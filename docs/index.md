# ATLAS Flavour Tagging Documentation

Welcome to the ATLAS Flavour-Tagging group page.
This replaces the old [FTAG Twiki](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/FlavourTagging?noredirect=on).

!!!tip "Doing physics? [Click here for recommendations!](recommendations/calib/intro_for_analysts.md)"
!!!bug "Find a bug? [report it on JIRA](https://its.cern.ch/jira/projects/AFT)"

The FTAG group is split into four subgroups:

- The [algorithms subgroup][ftag-algorithms-documentation] develops and maintains the heavy flavour tagging algorithms.
- The [calibrations subgroup](https://ftag-calib.docs.cern.ch) measures the efficiency of the algorithms in data and simulated MC events and provides corrections to the simulated events for use in analysis.
- The [software subgroup](https://ftag-sw.docs.cern.ch) maintains the software underlying these efforts.
- The [Xbb subgroup](https://xbb-docs.docs.cern.ch) studies algorithms and their calibration for identifying large-radius jets, such as Higgs-jets decaying to b- and c-quarks.

This website contains an overview of the group and specific documentation of the algorithms, the other subgroups have dedicated sites which are linked on the left.


## General Information

### Conveners

- [Francesco Di Bello](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=9187)
- [Dan Guest](https://atlas-glance.cern.ch/atlas/membership/members/profile?id=5921)

to be reached via <atlas-perf-flavtag-conveners@cern.ch>

### E-groups & Mailing List

The group mailing list is:
<hn-atlas-flavourTagPerformance@cern.ch> [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-flavourTagPerformance) [\[archive\]](https://groups.cern.ch/group/hn-atlas-flavourTagPerformance)

If you want to follow group activities, and access certain resources, it is highly recommended to 
subscribe to the following egroups using the [CERN egroups webpage](https://e-groups.cern.ch/e-groups/EgroupsSearch.do).

- [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=hn-atlas-flavourTagPerformance) [\[archive\]](https://groups.cern.ch/group/hn-atlas-flavourTagPerformance) `hn-atlas-flavourTagPerformance@cern.ch`
- [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-btagging-algorithms) [\[archive\]](https://groups.cern.ch/group/atlas-cp-flavtag-btagging-algorithms) `atlas-cp-flavtag-btagging-algorithms@cern.ch`
- [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-calibrations) [\[archive\]](https://groups.cern.ch/group/atlas-cp-flavtag-calibrations) `atlas-cp-flavtag-calibrations@cern.ch`
- [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-software) [\[archive\]](https://groups.cern.ch/group/atlas-cp-flavtag-software) `atlas-cp-flavtag-software@cern.ch`
- [\[join\]](https://e-groups.cern.ch/e-groups/EgroupsSubscription.do?egroupName=atlas-cp-flavtag-jetetmiss-BoostedXbbTagging) [\[archive\]](https://groups.cern.ch/group/atlas-cp-flavtag-jetetmiss-BoostedXbbTagging) `atlas-cp-flavtag-jetetmiss-BoostedXbbTagging@cern.ch`


### Meetings
FTAG Plenary meetings take place every Tuesday at 15h30 CERN time [[indico]](https://indico.cern.ch/category/9120/).

Overviews of all pleneary and subgroup meetings can be found in the left side-bar.


### Mattermost
You can join our Mattermost teams here:

[Algorithms + Software](https://mattermost.web.cern.ch/signup_user_complete/?id=ektad7hj4fdf5nehdmh1js4zfy){ .md-button .md-button--primary }
[Calibrations](https://mattermost.web.cern.ch/signup_user_complete/?id=9xp856ytcj87tqwfjs3g17rywc&md=link&sbr=su){ .md-button .md-button--primary }
[Xbb](https://mattermost.web.cern.ch/signup_user_complete/?id=fdodok94p3nhmxmkipo7z9uxkc&md=link&sbr=su){ .md-button .md-button--primary }

They are used to discuss a variety of topics and is the easiest way to ask questions.


### JIRA

The main ATLAS Flavour Tagging (AFT) Jira project can be found [here](https://its.cern.ch/jira/projects/AFT/).
An overview of open issues and AQPs in the group can be found at [this JIRA dashboard](https://its.cern.ch/jira/secure/Dashboard.jspa?selectPageId=22701).


### Contacts

Note that we're seeking replacements for some of these positions, please consider volunteering.

#### FTAG Derivation contacts

- Soumyananda Goswami <soumyananda.goswami@cern.ch>
- Wasikul Islam <wasikul.islam@cern.ch>

#### Central Data Derivation contacts

- Flavia De Almeida Dias <flavia.dias@cern.ch>
- Matthew Henry Klein <matthew.henry.klein@cern.ch>

#### MC and PMG

- Yi Yu <yi.yu@cern.cn>
- Qibin Liu <qibin.liu@cern.ch>

See [the dedicated page](samples/mc_requests.md) for details on making a request.

#### Physics Validation

- **(0.3 FTE)** Jacob Crosby <jacob.edwin.crosby@cern.ch> **new contact wanted!**
- Xuan Yang <xuan.yang@cern.ch>

#### Disk Space Manager

- **(0.1 FTE)** Bingxuan Liu <bingxuan.liu@cern.ch> **new concact wanted!** 


## Contributing

The documentation source is hosted on GitLab in the 
[ftag-docs](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/ftag-docs/) 
repository.

[ccd]: https://how-to.docs.cern.ch
[ccd-mkdocs]: https://how-to.docs.cern.ch/gitlabpagessite/create_site/creategitlabrepo/create_with_mkdocs/

We encourage anyone to contribute to these documentation pages by opening a merge request there.
To serve the docs locally, you can clone the repo and use `mkdocs` see your changes.

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/algorithms/ftag-docs.git
cd ftag-docs
python -m pip install -r requirements
mkdocs serve
```

You will see a link to open a locally hosted version of the documentation pages in a browser.
These will auto-update with changes to the source files.

## Forking this Page

To host your own version of this page at CERN, see CERN's [how-to.docs.cern.ch][ccd], specifically [the page on how to set up a MkDocs instance][ccd-mkdocs].

