# FTAG Workshops 

You can find links to the ATLAS FTAG Workshops below.

- **2024:** 7-14 Sept | Genoa (further details to be provided)
- [**2023:** 25 to 9 September | CERN](https://indico.cern.ch/event/1311519/)
- [**2022:** 31 October to 4 November | Amsterdam](https://indico.cern.ch/event/1193206/)
- [**2019** 7 to 9 June | DESY](https://indico.cern.ch/event/795039/)
