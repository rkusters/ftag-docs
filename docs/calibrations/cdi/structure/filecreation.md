# Creating a CDI file

Even though in most cases, the recommended CDI files stored in **CVFMS** should suffice for any physics analysis making use of the available [flavour tagging algorithms](https://ftag-docs.docs.cern.ch/algorithms/), you may still find a need to make your own custom CDI file.


The [`comb_CDI`](https://gitlab.cern.ch/atlas-ftag-calibration/comb_CDI) repository is what ATLAS uses to create CDI files. This repository implements a CI/CD Gitlab automated pipeline to run the CDI file creation scripts. The READ.ME file gives a comprehensive description of this procedure, and how one can use it directly to create CDI files. However, should you want to make more radical changes and implement entirely new functionality into a calibration file, then forking the repository and implementing it yourself is your best bet.