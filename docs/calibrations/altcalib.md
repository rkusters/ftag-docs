# Alternative calibrations

These calibration analyses are not yet included in the official CDI file.

| Jet Flavour | Process | Status | Aim | Team |
| ----------- | ------- | ------ | --- | ---- | 
| b-jets pTrel| multijet| R24 active | improve low pT uncertainty | Hieu Le, Binbin Dong, Zihan Zhang, Bingxuan Liu, Valentina Vecchio| 
| b-jets high pt| multijet| R24 active | extend pT range | Bingxuan Liu | 
| light-jets Adjusted MC | multijet | R24 active | cross-check of mainstream SFs | Vid Homsak | 
| light-jets Direct Tag | Z+jets | R24 active | Calibration without FlipTagger |  Storm Lin, Giacinto Piacquadio| 
| b-jets Optimal Trasport | ttbar dilep | R24 migration started | Continuous calibration | Malte Algren, Chris Pollard, Tobias Golling, Johnny Raine, Kinga Wozniak (R24) |
| b-jets | ttbar l+jets  | Still in R21 | extend pT range | Salvatore Fazio| 
| c-jets | W+c | R24 migration started | cross-check of mainstream SFs, softmuons | Benedikt Gocke (R21), Lucrezia Boccardo (R24) | 
| light-jets | multijet | Still in R21 | cross-check of mainstream SFs | Ellen Riefel | 
