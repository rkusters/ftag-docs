from copy import deepcopy


def get(data: dict, key: str, default=None):
    """
    Get object from dictionary, return None object if key not present.

    Parameters
    ----------
    data : dict
        dictionary containing desired object
    key : str
        name of desired object
    default :
        object which is returned if key is not present in dictionary
    """
    if key in data:
        if data[key]:
            return deepcopy(data[key])
        else:
            return default
    else:
        return default


def dump_sample_information_md(name: str, data: dict, level: int, selection: set):
    """
    Convert sample information to markdown string and return it.

    Parameters
    ----------
    name : str
        name of sample list (can also be empty)
    data : dict
        nested dictionary containing sample information
    level : int
        level of indentation
    selection: set
        restrict selection to certain samples only which are part of the set
    """
    samples = get(data, "samples", {})
    jira = get(data, "jira")
    comments = get(data, "comments")
    h = "#" * level
    do_return_list = False
    output_str = f"""{h} {name}

| Sample | MC campaign | H5 ntuple | DAOD  | TDD git hash |
| ------ | ----------- | --------- | ----- | ------------ |
"""
    for sample, data in samples.items():
        if selection and (sample not in selection):
            continue
        do_return_list = True
        for campaign, values in data.items():
            h5 = get(values, "h5", "n/a")
            daod = get(values, "daod", "n/a")
            tdd = get(values, "tdd", "n/a")
            if tdd != "n/a":
                tdd = f"[{tdd[:8]}](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/commit/{tdd})"
            output_str += f"| {sample} | {campaign} | {h5} | {daod} | {tdd} |\n"

    output_str += "\n\n"
    output_str += "**Comments**\n\n"
    for comment in comments:
        output_str += f"- {comment}\n"
    output_str += "\n\n"

    output_str += "**Jira tickets**\n\n"
    for link in [f"[{url_to_name(url)}]({url})" for url in jira]:
        output_str += f"- {link}\n"

    # if sample list is not empty, return list, otherwise just empty line
    if do_return_list:
        return output_str
    else:
        return ""


def url_to_name(url):
    """
    Extract Jira ticket name or production system task from URL

    Parameters
    ----------
    name : url
        URL to Jira ticket or production system task
    """
    return url.rstrip("/").split("/")[-1]


def sample_to_admonition(sample_info, style="info"):
    """
    Modify and indent sample information string such that it is displayed
    in rendered markdown as an info box which can be toggled.

    Parameters
    ----------
    name : sample_info
        string with sample info
    """
    output = ""
    for i, line in enumerate(sample_info.split("\n")):
        line = line.replace("#", "").strip()
        if i == 0:
            output += f'??? {style} "{line}"\n'
        else:
            output += "    " + line + "\n"
    return output


def get_changelog_entry(name: str, data: dict):
    """
    Extract information needed for creating a changelog
    from samplelist file and return it as string.

    Parameters
    ----------
    name : str
        name of sample list
    data : dict
        nested dictionary containing sample information
    """
    comments = get(data, "comments")
    jira = get(data, "jira")

    comments_str = ". ".join(comments)
    jira_str = ", ".join([f"[{url_to_name(url)}]({url})" for url in jira])

    # | p-tag | Comments | Jira |
    output_str = f"| {name} | {comments_str} | {jira_str} |"

    return output_str


def dump_aod_information_md(data: dict):
    """
    Convert sample information to markdown string and return it.

    Parameters
    ----------
    data : dict
        dictionary containing sample information
    """

    output_str = """
| Sample | MC campaign | AOD  | events | prodtask |
| ------ | ----------- | ---- | ------ | -------- |
"""
    for sample, sample_data in data.items():
        for campaign, values in sample_data.items():
            aod = get(values, "aod", "n/a")
            events = get(values, "events", "n/a")
            prodtask = get(values, "prodtask", "n/a")
            if prodtask != "n/a":
                prodtask = f"[{prodtask}](https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/{prodtask})"
            output_str += f"| {sample} | {campaign} | {aod} | {events} | {prodtask} |\n"

    output_str += "\n\n"
    return output_str
